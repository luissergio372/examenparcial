from django.apps import AppConfig


class AutentificacioncustomConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'autentificacioncustom'
